#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat() {
};

Bureaucrat::Bureaucrat(std::string name, int grade) :
	_name(name)
{
		if (grade < 1) {
			throw GradeTooHighException();
		}
		else if (grade > 150) {
			throw GradeTooLowException();
		}
		this->_grade = grade;
}

Bureaucrat::Bureaucrat(const Bureaucrat &src) {
	*this = src;
}

Bureaucrat::~Bureaucrat() {
}

Bureaucrat &Bureaucrat::operator=(const Bureaucrat &rhs) {
	this->_grade = rhs._grade;

	return *this;
}

void Bureaucrat::raiseGrade() {
	if (this->_grade == 1){
		throw GradeTooHighException();
	}
	else {
		this->_grade--;
	}
}

void Bureaucrat::lowerGrade() {
	if (this->_grade == 150){
		throw GradeTooLowException();
	}
	else {
		this->_grade++;
	}

}

std::string const Bureaucrat::getName()  const{
	return this->_name;
}

int 	Bureaucrat::getGrade() const {
	return this->_grade;
}

std::ostream &operator<<(std::ostream &out, const Bureaucrat &bureaucrat) {
	out << ("<" + bureaucrat.getName() + ">, bureaucrat grade <") << bureaucrat.getGrade() << ">";

	return out;
}


const char* Bureaucrat::GradeTooHighException::what() const throw(){
	return "Grade too high";
}


const char* Bureaucrat::GradeTooLowException::what() const throw(){
	return "Grade too low";
}
