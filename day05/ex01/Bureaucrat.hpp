#ifndef CPP_BOOTCAMP_BUREAUCRAT_H
#define CPP_BOOTCAMP_BUREAUCRAT_H

#include <exception>
#include <string>
#include <iostream>

#include "Form.hpp"

class Bureaucrat {

public:
	Bureaucrat();
	Bureaucrat(std::string name, int grade);
	Bureaucrat(const Bureaucrat &src);
	~Bureaucrat();
	Bureaucrat &operator=(const Bureaucrat &rhs);

	std::string const getName() const;
	int getGrade() const;

	void raiseGrade();
	void lowerGrade();
	void signForm(Form &form);

	class GradeTooHighException : public std::exception {
	public:
		virtual const char* what() const throw();
	};

	class GradeTooLowException : public std::exception{
	public:
		virtual const char* what() const throw();
	};

private:
	std::string const	_name;
	int					_grade;
};

std::ostream &operator<<(std::ostream &out, const Bureaucrat &bureaucrat);

#endif //CPP_BOOTCAMP_BUREAUCRAT_H
