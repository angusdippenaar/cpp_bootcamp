/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Team.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/28 13:45:35 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/28 13:45:36 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Team.hpp"

Team::Team() : Character(), team(randomTeam()) {
	std::cout << "<ANNOUNCER>The recently summoned has joined the " << this->team << "s.\n";
	oppositeTeam();
}

Team::Team(std::string name) : Character(name), team(randomTeam()) {
	std::cout << "<ANNOUNCER>The recently spawned has joined the " << this->team << "s.\n";
	oppositeTeam();
}

Team::Team(std::string name, std::string team) : Character(name), team(team) {
	std::cout << "<ANNOUNCER>The recently spawned has joined the " << this->team << "s.\n";
	oppositeTeam();
}

Team::Team(Team const & src) {
	*this = src;
	std::cout << "<ANNOUNCER>The recently cloned has joined the " << this->team << "s.\n";
}

Team::~Team() {
	std::cout << "<ANNOUNCER>A " << this->team << " has left the team.\n";
}
Team & 	Team::operator=(Team const & src) {
	this->name = src.getName();
	this->team = src.getTeam();
	this->enemyTeam = src.getEnemyTeam();
	return (*this);
}

std::string	Team::getTeam() const {
	return (this->team);
}

void		Team::setTeam(std::string team) {
	this->team = team;
}

std::string	Team::getEnemyTeam() const {
	return (this->enemyTeam);
}

void		Team::setEnemyTeam(std::string team) {
	this->enemyTeam = team;
}

std::string     Team::randomTeam() const {
    std::string names[] = {"Goodie", "Baddie", "Goodie", "Baddie", "Goodie", "Baddie", "Goodie", "Baddie", "Goodie", "Baddie"};
    return (names[(rand() % 10)]);
}

void	     Team::oppositeTeam() {
	//std::cout << "Testing Team: " << this->team <<std::endl;
    if (this->team.compare("Baddie") != 0) {
    	this->enemyTeam = "Baddie";
    	//std::cout << "In Baddie: " << this->enemyTeam <<std::endl;
    }
    else {
    	this->enemyTeam = "Goodie";
    	//std::cout << "In Goodie: " << this->enemyTeam <<std::endl;
    }
}