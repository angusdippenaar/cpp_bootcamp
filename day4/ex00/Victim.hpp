/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:08:15 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:08:16 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
#define VICTIM_HPP

#include "Team.hpp"
#include "Character.hpp"

class Victim : public Team
{
public:
	Victim();
	Victim(std::string name);
    Victim(std::string name, std::string team);
    Victim(Victim const & src);
    ~Victim();
    Victim & 		operator=(Victim const & src);
    virtual void	getPolymorphed() const;
};

std::ostream &  operator<<( std::ostream & o, Victim const & rhs);

#endif