/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:08:05 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:08:07 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

Peon::Peon() : Victim() {
	std::cout << "Zog zog.\n";
}

Peon::Peon(std::string name) : Victim(name) {
	std::cout << "Zog zog.\n";
}

Peon::Peon(std::string name, std::string team) : Victim(name, team) {
	std::cout << "Zog zog.\n";
}

Peon::Peon(Peon const & src) {
	*this = src;
	std::cout << "Zog zog.\n";
}

Peon::~Peon() {
	std::cout << "Bleuark...\n";
}

Peon &	Peon::operator=(Peon const & src) {
	this->name = src.getName();
	this->team = src.getTeam();
	this->enemyTeam = src.getEnemyTeam();
	return (*this);
}

void		Peon::getPolymorphed() const {
	std::cout << this->name << " has been turned into a pink pony !\n";
}
