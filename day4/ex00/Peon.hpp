/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:08:10 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:08:12 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"

class Peon : public Victim
{
public:
	Peon();
	Peon(std::string name);
	Peon(std::string name, std::string team);
	Peon(Peon const & src);
	~Peon();
	Peon & 		operator=(Peon const & src);
	void		getPolymorphed() const;
};

#endif