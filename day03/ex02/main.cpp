#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main() {
	FragTrap	frag = FragTrap("Fraggers");
	ScavTrap	scav = ScavTrap("Scavvers");

	scav.challengeNewcomer();

	frag.meleeAttack("Scavvers");
	scav.takeDamage(30);

	scav.rangedAttack("Fraggers");
	frag.takeDamage(15);

	frag.beRepaired(10);
	scav.beRepaired(50);

	frag.vaulthunter_dot_exe("Scavvers");
}