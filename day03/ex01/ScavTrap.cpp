#include "ScavTrap.hpp"

std::string const ScavTrap::_challenges[N_CHALLENGES] = {
		"Fight me",
		"The human eye can only see 30fps",
		"1v1 me IRL Bayside KFC",
		"Your mother was a murloc",
		"You're literally a scrub"
};

ScavTrap::ScavTrap() :
		_hp(100),
		_maxHP(100),
		_energy(50),
		_maxEnergy(50),
		_level(1),
		_name("The Nameless One"),
		_meleeDamage(20),
		_rangedDamage(15),
		_armour(3) {
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(SC4V-TP) " + this->_name + " is constructed\n";
};

ScavTrap::ScavTrap(std::string name) :
		_hp(100),
		_maxHP(100),
		_energy(50),
		_maxEnergy(50),
		_level(1),
		_meleeDamage(20),
		_rangedDamage(15),
		_armour(3) {
	this->_name = name;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(SC4V-TP) " + this->_name + " is constructed\n";
};

ScavTrap::ScavTrap(ScavTrap const &src) :
		_hp(src._hp),
		_maxHP(src._maxHP),
		_energy(src._energy),
		_maxEnergy(src._maxEnergy),
		_level(src._level),
		_name(src._name),
		_meleeDamage(src._meleeDamage),
		_rangedDamage(src._rangedDamage),
		_armour(src._armour) {
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(SC4V-TP) " + this->_name + " is replicated\n";
};

ScavTrap::~ScavTrap() {
	std::cout << "RIP (SC4V-TP) " + this->_name + "\n";
};

ScavTrap &ScavTrap::operator=(const ScavTrap &rhs) {
	_hp = rhs._hp;
	_maxHP = rhs._maxHP;
	_energy = rhs._energy;
	_maxEnergy = rhs._maxEnergy;
	_level = rhs._level;
	_name = rhs._name;
	_meleeDamage = rhs._meleeDamage;
	_rangedDamage = rhs._rangedDamage;
	_armour = rhs._armour;

	return *this;
};

void ScavTrap::beRepaired(unsigned int amount) {
	if (this->_hp + amount > this->_maxHP)
		amount = this->_maxHP - this->_hp;

	std::cout << "(SC4V-TP) " + this->_name + " is being repaired for " << amount << "\n";

	this->_hp += amount;
}

void ScavTrap::meleeAttack(const std::string &target) const {
	std::cout << "(SC4V-TP) " + this->_name + " melee attacks " + target + ", dealing " << this->_meleeDamage << " points of damage!\n";
}

void ScavTrap::rangedAttack(const std::string &target) const {
	std::cout << "(SC4V-TP) " + this->_name + " range attacks " + target + ", dealing " << this->_meleeDamage << " points of damage!\n";
}

unsigned int max(int i1, int i2) {
	return static_cast<unsigned int>(i1 > i2 ? i1 : i2);
}

void ScavTrap::takeDamage(unsigned int amount) {
	this->_hp = max(this->_hp - max(amount - this->_armour, 0), 0);
	std::cout << "(SC4V-TP) " + this->_name + " takes "  << amount << " points of damage! | Remaing health: " << this->_hp << '\n';
}

void ScavTrap::challengeNewcomer() {
	std::cout << this->_challenges[rand() % N_CHALLENGES] << '\n';
};
