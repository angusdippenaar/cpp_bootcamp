#ifndef CPP_BOOTCAMP_SCAVTRAP_H
# define CPP_BOOTCAMP_SCAVTRAP_H

#include <string>
#include <iostream>
#include <cstdlib>

const unsigned int N_CHALLENGES = 5;

class ScavTrap {

public:

	ScavTrap();
	ScavTrap(ScavTrap const &src);
	ScavTrap(std::string name);
	~ScavTrap();
	ScavTrap & operator=(const ScavTrap &rhs);

	void	rangedAttack(std::string const &target) const;
	void	meleeAttack(std::string const &target) const;
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	void 	challengeNewcomer();

private:

	unsigned int	_hp;
	unsigned int	_maxHP;
	unsigned int	_energy;
	unsigned int	_maxEnergy;
	unsigned int	_level;
	std::string		_name;
	unsigned int	_meleeDamage;
	unsigned int	_rangedDamage;
	unsigned int	_armour;


	static std::string const _challenges[N_CHALLENGES];
};

#endif
