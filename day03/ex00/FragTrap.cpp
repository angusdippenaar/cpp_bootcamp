#include "FragTrap.hpp"

const std::string FragTrap::_actions[nActions] = {
		"Clap-In-The-Box",
		"Gun Wizard",
		"Torgue Fiesta",
		"Pirate Ship Mode",
		"One Shot Wonder"
};

const std::string FragTrap::_sayings[nSayings] = {
		"Are... are you my father?",
		"Are you god? Am I dead?",
		"O-KAY! Thanks for giving me a second chance, God. I really appreciate it.",
		"Hey everybody! Check out my package!",
		"Let's get this party started!",
		"Glitching weirdness is a term of endearment, right?",
		"Recompiling my combat code!",
		"This time it'll be awesome, I promise!",
		"Look out everybody! Things are about to get awesome!"
};

FragTrap::FragTrap(void) :
		_hp(100),
		_maxHP(100),
		_energy(100),
		_maxEnergy(100),
		_level(1),
		_name("FR4G-TP"),
		_meleeDamage(30),
		_rangedDamage(20),
		_armour(5) {
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "New Claptrap robot constructed with name: " << this->_name << std::endl;
	std::cout << "\"" << this->_sayings[std::rand() % nSayings] << "\"" << std::endl;
}

FragTrap::FragTrap(const std::string name) :
		_hp(100),
		_maxHP(100),
		_energy(100),
		_maxEnergy(100),
		_level(1),
		_name(name),
		_meleeDamage(30),
		_rangedDamage(20),
		_armour(5) {
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "New Claptrap robot constructed with name: " << this->_name << std::endl;
	std::cout << "\"" << this->_sayings[std::rand() % nSayings] << "\"" << std::endl;
}

FragTrap::FragTrap(FragTrap const &src) :
		_hp(src._hp),
		_maxHP(src._maxHP),
		_energy(src._energy),
		_maxEnergy(src._maxEnergy),
		_level(src._level),
		_name(src._name),
		_meleeDamage(src._meleeDamage),
		_rangedDamage(src._rangedDamage),
		_armour(src._armour) {
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "New Claptrap robot constructed with name: " << this->_name << std::endl;
	std::cout << "\"" << this->_sayings[std::rand() % nSayings] << "\"" << std::endl;
}

FragTrap::~FragTrap() {
	std::cout << this->_name << " the Claptrap has been deconstructed for parts" << std::endl;
}

void	FragTrap::beRepaired(unsigned int amount) {
	if (this->_hp + amount > this->_maxHP)
		amount = this->_maxHP - this->_hp;

	std::cout << "FR4G-TP " + this->_name + " is being repaired for " << amount << std::endl;

	this->_hp += amount;
}

void	FragTrap::meleeAttack(const std::string &target) const {
	std::cout << "FR4G-TP " + this->_name + " melee attacks " << target << ", dealing " << this->_meleeDamage << " points of damage!" << std::endl;
}

void	FragTrap::rangedAttack(const std::string &target) const {
	std::cout << "FR4G-TP " + this->_name + " range attacks " << target << ", dealing " << this->_rangedDamage << " points of damage!" << std::endl;
}

unsigned int max(int i1, int i2) {
	return static_cast<unsigned int>(i1 > i2 ? i1 : i2);
}

void	FragTrap::takeDamage(unsigned int amount) {
	this->_hp = max(this->_hp - max(amount - this->_armour, 0), 0);

	std::cout << "FR4G-TP " << this->_name << " took " << amount << " damage. " << this->_hp << "HP left" << std::endl;
}

void	FragTrap::vaulthunter_dot_exe(std::string const & target) {
	std::cout << "FR4G-TP " + this->_name + " special attacks ( " + this->_actions[std::rand() % nActions] + " ) " << target << std::endl;

	this->_energy -= max(this->_energy - 25, 0);
}

FragTrap &FragTrap::operator=(const FragTrap &rhs) {
	this->_hp = rhs._hp;
	this->_maxHP = rhs._maxHP;
	this->_energy = rhs._energy;
	this->_maxEnergy = rhs._maxEnergy;
	this->_level = rhs._level;
	this->_name = rhs._name;
	this->_meleeDamage = rhs._meleeDamage;
	this->_rangedDamage = rhs._rangedDamage;
	this->_armour = rhs._armour;

	return *this;
};

void	FragTrap::setName(const std::string newName) {
	this->_name = newName;
}

