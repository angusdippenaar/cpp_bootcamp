#include "FragTrap.hpp"

int main() {
	FragTrap	frag = FragTrap("Desmond");
	FragTrap	*frag2 = new FragTrap(frag);
	FragTrap	frag3;

	frag3 = frag;

	frag2->setName("Barnacle");
	frag3.setName("Hercules");

	frag.meleeAttack("Barnacle");
	frag2->takeDamage(30);

	frag3.rangedAttack("Barnacle");
	frag2->takeDamage(20);

	frag2->vaulthunter_dot_exe("Desmond and Hercules");
	frag.takeDamage(200);
	frag3.takeDamage(80);

	frag2->beRepaired(60);
	frag3.beRepaired(60);

	delete frag2;

	return 0;
}