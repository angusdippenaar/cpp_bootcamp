#ifndef CPP_BOOTCAMP_FRAGTRAP_HPP
#define CPP_BOOTCAMP_FRAGTRAP_HPP

#include <string>
#include <iostream>
#include <cstdlib>

#include "ClapTrap.hpp"

const unsigned int N_ACTIONS = 5;
const unsigned int N_SAYINGS = 9;

class FragTrap : virtual public ClapTrap {
public:
	FragTrap(void);
	FragTrap(const std::string name);
	FragTrap(FragTrap const &src);
	~FragTrap(void);

	void	initFrag(std::string name);
	void	vaulthunter_dot_exe(const std::string &target);

private:
	static const std::string _actions[];
	static const std::string _sayings[];
};

#endif //CPP_BOOTCAMP_FRAGTRAP_HPP
