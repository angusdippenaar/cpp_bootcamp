#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int main() {
	FragTrap	frag	= FragTrap("Fraggers");
	ScavTrap	scav	= ScavTrap("Scavvers");
	NinjaTrap	ninja	= NinjaTrap("Ninjas");
	SuperTrap	super	= SuperTrap("Supes");


	scav.challengeNewcomer();

	frag.meleeAttack("Scavvers");
	scav.takeDamage(30);

	scav.rangedAttack("Fraggers");
	frag.takeDamage(15);

	frag.beRepaired(10);
	scav.beRepaired(50);

	frag.vaulthunter_dot_exe("Scavvers");

	ninja.ninjaShoebox(frag);
	ninja.ninjaShoebox(scav);
	ninja.ninjaShoebox(ninja);

	super.ninjaShoebox(scav);
	super.vaulthunter_dot_exe("Scavvers");
}