#ifndef CPP_BOOTCAMP_SCAVTRAP_H
# define CPP_BOOTCAMP_SCAVTRAP_H

#include <string>
#include <iostream>
#include <cstdlib>

#include "ClapTrap.hpp"

const unsigned int N_CHALLENGES = 5;


class ScavTrap : public ClapTrap {
public:
	ScavTrap(void);
	ScavTrap(ScavTrap const &src);
	ScavTrap(std::string name);
	~ScavTrap(void);

	void	initScav(std::string name);
	void 	challengeNewcomer();

private:
	static std::string const _challenges[N_CHALLENGES];
};
#endif
