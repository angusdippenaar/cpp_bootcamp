#ifndef CPP_BOOTCAMP_NINJATRAP_H
#define CPP_BOOTCAMP_NINJATRAP_H

#include <string>
#include <iostream>
#include <cstdlib>

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

class NinjaTrap : public ClapTrap {

public:
	NinjaTrap();
	NinjaTrap(const NinjaTrap &src);
	NinjaTrap(const std::string name);
	~NinjaTrap();

	void initNinja(std::string name);
	void ninjaShoebox(const ScavTrap &target);
	void ninjaShoebox(const FragTrap &target);
	void ninjaShoebox(const NinjaTrap &target);
	void ninjaShoebox(const ClapTrap &target);

private:
};


#endif //CPP_BOOTCAMP_NINJATRAP_H
