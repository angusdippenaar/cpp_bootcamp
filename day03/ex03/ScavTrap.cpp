#include "ScavTrap.hpp"

std::string const ScavTrap::_challenges[N_CHALLENGES] = {
		"Fight me",
		"The human eye can only see 30fps",
		"1v1 me IRL Bayside KFC",
		"Your mother was a murloc",
		"You're literally a scrub"
};

ScavTrap::ScavTrap() {
	initScav("The Nameless One");
}

ScavTrap::ScavTrap(std::string name) {
	initScav(name);
}

void ScavTrap::initScav(std::string name) {
	this->_hp = 100;
	this->_maxHP = 100;
	this->_energy = 50;
	this->_maxEnergy = 50;
	this->_name = name;
	this->_type = "SC4V-TP";
	this->_level = 1;
	this->_meleeDamage = 20;
	this->_rangedDamage = 15;
	this->_armour = 3;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is constructed\n";
}

ScavTrap::ScavTrap(ScavTrap const &src) {
	*this = src;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is replicated\n";
}

ScavTrap::~ScavTrap() {
	std::cout << "(" + this->_type + ") " + this->_name + " the ScavTrap has been deconstructed for parts\n";
}

void ScavTrap::challengeNewcomer() {
	std::cout << "(" + this->_type + ") " + this->_name + " \"" << this->_challenges[rand() % N_CHALLENGES] << "\"\n";
}
