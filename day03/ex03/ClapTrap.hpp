#ifndef CPP_BOOTCAMP_CLAPTRAP_H
#define CPP_BOOTCAMP_CLAPTRAP_H

#include <string>
#include <iostream>
#include <cstdlib>

unsigned int max(int i1, int i2);

class ClapTrap {

public:
	ClapTrap(void);
	ClapTrap(const ClapTrap &src);
	~ClapTrap(void);
	ClapTrap &operator=(const ClapTrap &rhs);

	void	rangedAttack(const std::string &target) const;
	void	meleeAttack(const std::string &target) const;
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	void	setName(const std::string newName);
	const std::string getName(void) const;

protected:
	unsigned int	_hp;
	unsigned int	_maxHP;
	unsigned int	_energy;
	unsigned int	_maxEnergy;
	unsigned int	_level;
	std::string		_name;
	std::string		_type;
	unsigned int	_meleeDamage;
	unsigned int	_rangedDamage;
	unsigned int	_armour;
};


#endif