#include "ClapTrap.hpp"

ClapTrap::ClapTrap(void) {
	std::cout << "A Clatrap is constructed\n";
}

ClapTrap::ClapTrap(const ClapTrap &src) {
	*this = src;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is replicated\n";
}

ClapTrap::~ClapTrap(void) {
	std::cout << "(" + this->_type + ") " + this->_name << " the ClapTrap has been deconstructed for parts\n";
}

ClapTrap &ClapTrap::operator=(const ClapTrap &rhs) {
	_hp = rhs._hp;
	_maxHP = rhs._maxHP;
	_energy = rhs._energy;
	_maxEnergy = rhs._maxEnergy;
	_level = rhs._level;
	_name = rhs._name;
	_meleeDamage = rhs._meleeDamage;
	_rangedDamage = rhs._rangedDamage;
	_armour = rhs._armour;

	return *this;
}

void	ClapTrap::beRepaired(unsigned int amount) {
	if (this->_hp + amount > this->_maxHP)
		amount = this->_maxHP - this->_hp;

	std::cout << "(" + this->_type + ") " + this->_name  + " is being repaired for " << amount << '\n';

	this->_hp += amount;
}

void	ClapTrap::meleeAttack(const std::string &target) const {
	std::cout << "(" + this->_type + ") " + this->_name + " melee attacks " << target << ", dealing " << this->_meleeDamage << " points of damage!\n";
}

void	ClapTrap::rangedAttack(const std::string &target) const {
	std::cout << "(" + this->_type + ") " + this->_name + " range attacks " << target << ", dealing " << this->_rangedDamage << " points of damage!\n";
}

unsigned int max(int i1, int i2) {
	return static_cast<unsigned int>(i1 > i2 ? i1 : i2);
}

void	ClapTrap::takeDamage(unsigned int amount) {
	this->_hp = max(this->_hp - max(amount - this->_armour, 0), 0);

	std::cout << "(" + this->_type + ") " + this->_name + " took " << amount << " damage. " << this->_hp << "HP left\n";
}

void	ClapTrap::setName(const std::string newName) {
	this->_name = newName;
}

const std::string ClapTrap::getName(void) const {
	return this->_name;
}