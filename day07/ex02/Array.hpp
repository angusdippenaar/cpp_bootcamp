#ifndef CPP_BOOTCAMP_ARRAY_H
#define CPP_BOOTCAMP_ARRAY_H

#include <stdexcept>
#include <cstddef>

template<class T>
class Array {

public:

	T **arr;

	Array()
	{
		this->arr = NULL;
		this->_size = 0;
	}

	Array(unsigned int n)
	{
		this->arr = new T * [n];
		this->_size = n;
		for(unsigned int i = 0; i < n; i++) {
			arr[i] = new T;
		}
	}

	Array(const Array &src)
	{
		*this = src;
	}

	~Array() {
		if (_size > 0) {
			for(unsigned int i = 0; i < this->_size; i++) {
				if (arr[i] != NULL) {
					delete this->arr[i];
				}
			}
			delete [] arr;
		}
	}

	Array<T> &operator=(const Array &rhs)
	{
		if (_size > 0) {
			for(unsigned int i = 0; i < this->_size; i++) {
				if (arr[i] != NULL) {
					delete this->arr[i];
				}
			}
			delete [] arr;
		}

		arr = new T * [rhs.size()];
		_size = rhs.size();
		for (unsigned int i = 0; i < this->_size; i++) {
			arr[i] = new T;
			*arr[i] = *rhs.arr[i];
		}

		return *this;
	}

	T &operator[](int x)
	{
		if (static_cast<unsigned int>(x) <= this->_size)
		{
			return *arr[x];
		}
		throw std::out_of_range("Array out of bounds");
	}

	unsigned int size() const
	{
		return this->_size;
	}

private:

	unsigned int _size;
};


#endif //CPP_BOOTCAMP_ARRAY_H
