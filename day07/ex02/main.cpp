#include <iostream>
#include <string>

#include "Array.hpp"

int main() {
	Array<int> arr = Array<int>(20);

	for (unsigned int i = 0; i < arr.size(); i++){
		arr[i] = i;
	}

	for (unsigned int i = 0; i < arr.size(); i++){
		std::cout << arr[i] << ", ";
	}

	std::cout << '\n';

	try {
		std::cout << arr[69];
	}
	catch (std::exception &e){
		std::cout << e.what() << '\n';
	}


	return 0;
}