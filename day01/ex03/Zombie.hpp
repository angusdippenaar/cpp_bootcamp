#ifndef ZOMBIE_H
# define ZOMBIE_H

#include <string>
#include <iostream>

class Zombie {

public:

	Zombie(std::string type, std::string name);

	~Zombie();

	void announce();

private:

	std::string _type;
	std::string _name;

};

#endif