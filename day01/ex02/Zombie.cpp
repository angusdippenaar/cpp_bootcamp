#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) {
    this->_name = name;
    this->_type = type;

    std::cout << this->_name << " the " << this->_type << " zombie appeared\n";
}

Zombie::~Zombie() {
    std::cout << "R.I.P. " << this->_name << " the " << this->_type << " zombie\n";    
}

void Zombie::announce() {
    std::cout << this->_name << " the " << this->_type << " zombie says: Braiiiiiiinnnssss...\n";
}
