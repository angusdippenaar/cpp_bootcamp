#include "Pony.hpp"

Pony::Pony(std::string name, std::string color) {

    this->name = name;
    std::cout << "My name is " << name << " and I am a " << color << " pony!\n";
}

Pony::~Pony() {
    std::cout << this->name << " says goodbye.\n";
}
